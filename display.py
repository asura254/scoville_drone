import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#importing the libraries to be used
#numpy is for conversion of the polar coordinates to rectangular
#pandas is to handle the csv file
#matplotlib is for the drawing

################################# convert the coordinates from Polar to rectangular
def Polar2Rec(tet,r):
    pi=np.pi
    x=np.cos(tet*pi/180)*(r/1000)
    y=np.sin(tet*pi/180)*(r/1000)
    return x,y
################################# Read 1 sweep set and save them to a list
def read_set(df,l,sw):
    st=int(df.index[df[0]==sw][0]) #get index of sweep number
    f=df[1][st]
    i=st+1
    r=df[1][i]
    tet=df[0][i]
    j=0
    while(j<f):
        coord=Polar2Rec(tet,r)
        l.append(coord)
        i+=1
        r=df[1][i]
        tet=df[0][i]
        j+=1
################################# Print a sweep. Initial form, will be enhanced later
def print_sw(l,bst,ax):
    x0=(bst[0]+bst[2])/2
    y0=(bst[1]+bst[3])/2
    i=0
    while (i<len(l)):
        ax.plot(l[i][0]-x0,l[i][1]-y0,'b.')
        i+=1


def print_sw2(l,sw,ax,df2): #different print function, taking into account flight path
    st=int(df2.index[df2[0]==0][0]) #find initial position
    x0=df2[0][st+1]
    y0=df2[1][st+1]
    st=int(df2.index[df2[0]==sw][0]) #find sweep index
    x1=df2[0][st+1]
    y1=df2[1][st+1]
    x0=x0-x1
    y0=y0-y1
    arr=np.array(l)
    x,y=arr.T
    x=x-x0 #adjust the values of the coordinates with the position of the drone
    y=y+y0
    ax.scatter(x,y,marker='.')
################################# Print the path of the drone
def print_path(df2,ax):
    st=int(df2.index[df2[0]==0][0])
    x0=df2[0][st+1]
    y0=df2[1][st+1]
    i=0
    while (i<68):
        if(df2[1][i]==1):
            i+=1
            continue
        ax.plot((df2[0][i]-x0)*-1,(df2[1][i]-y0)*-1,'ro')
        i+=1
################################# Printing a "sw" number os sweeps
def print_mul(df,df2,sw):
    i=0
    j=0
    l=[]
    fig = plt.figure()
    ax=fig.add_subplot(111) #enhancing plot quality
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.ylim(-15, 3)
    plt.xticks(range(-12, 12, 2), fontsize=14)
    plt.yticks(range(-15, 3, 2), fontsize=14)
    plt.ylabel("2D Room Y position", fontsize=16)
    plt.xlabel("2D Room X position", fontsize=16)
    plt.title("Room Layout with %i sweeps" %sw, fontsize=22)
    while(j<sw):
        read_set(df,l,j)
        print_sw2(l,j,ax,df2)
        l.clear()
        j+=1
    print_path(df2,ax)
    fig.show()
    #plt.pause(5)
    #plt.close()

############################## Main operational code goes here



df2=pd.read_csv('FlightPath.csv', header = None)
df=pd.read_csv('LIDARPoints.csv', header = None)
val=int(input("Enter 1 to see all the sweeps together alongside the flight path, or 0 to see a specific sweep"))
while (val==0):
    sw=int(input("Please enter a number from 0 to 33 to see a specific sweep or -1 to exit"))
    if(sw==-1):
        break
    elif( (0>sw) or (sw>33) ):
        print("Invalid input, please try again")
        continue
    l=[]
    read_set(df,l,sw)
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.ylim(-15, 3)
    plt.xticks(range(-12, 12, 2), fontsize=14)
    plt.yticks(range(-15, 3, 2), fontsize=14)
    plt.ylabel("2D Room Y position", fontsize=16)
    plt.xlabel("2D Room X position", fontsize=16)
    plt.title("Room Layout sweep number %i" %sw, fontsize=22)
    print_sw2(l,sw,ax,df2)
    fig.show()
    val=int(input("Please enter 0 to see another sweep or any other number to leave"))
while(val==1):
    sw=int(input("Please enter a number from 0 to 33 to see represent how many sweeps you want to see overlapped or -1 to exit"))
    if(sw==-1):
        break
    elif( (0>sw) or (sw>33) ):
        print("Invalid input, please try again")
        continue
    print_mul(df,df2,sw)
    val=int(input("Please enter 1 to see another sweep or any other number to leave"))





#print_mul(df,32)
