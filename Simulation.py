import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

############################## Convert the coordinates to Polar for the reading
def Rec2Polar(x,y):
    j=len(x)
    r=[]   #radi
    tet=[] #angle
    i=0
    pi=np.pi
    while (i<j):
        rt=np.sqrt(x[i]**2+y[i]**2)
        if (rt==0):
            tett=0
        else:
            tett=np.arcsin(y[i]/rt)
        if (tett in tet): #avoid same angle twice due to duality of sine functions
            tett+=pi/2
        tett=tett*180/pi
        if tett<0:  #if the value is negative just compensate with 360 to have all positives
            tett=360+tett
        tet.append(tett)
        r.append(rt)
        i+=1
    ret=[r,tet]
    return ret
########################## simple function to take the coordinates and translate them to the drone location
def write_reading(x,y,xs,ys):
    x=x-xs
    y=y-ys
    ret=Rec2Polar(x,y)
    return ret
##########################
#Creation of the room dimensions and plotting them
x1=np.linspace(-5,5,num=150)
y1=np.ones(150)*20
x1=np.append(x1,np.ones(150)*-5)
y1=np.append(y1,np.linspace(20,-10,num=150))
x1=np.append(x1,np.linspace(-5,11,num=50))
y1=np.append(y1,np.ones(50)*-10)
x1=np.append(x1,np.ones(150)*5)
y1=np.append(y1,np.linspace(20,0,num=150))
plt.scatter(x1,y1,marker='.')
x1s=0 #drone location during the sweep
y1s=17#drone location during the sweep
x2=np.linspace(-5,5,num=120)
y2=np.ones(120)*20
x2=np.append(x2,np.ones(150)*-5)
y2=np.append(y2,np.linspace(20,-10,num=150))
x2=np.append(x2,np.linspace(-5,13,num=80))
y2=np.append(y2,np.ones(80)*-10)
x2=np.append(x2,np.ones(150)*5)
y2=np.append(y2,np.linspace(20,0,num=150))
plt.scatter(x2,y2,marker='.')
x2s=0
y2s=10
x3=np.linspace(-5,5,num=100)
y3=np.ones(100)*20
x3=np.append(x3,np.ones(150)*-5)
y3=np.append(y3,np.linspace(20,-10,num=150))
x3=np.append(x3,np.linspace(-5,15,num=125))
y3=np.append(y3,np.ones(125)*-10)
x3=np.append(x3,np.ones(125)*5)
y3=np.append(y3,np.linspace(20,0,num=125))
plt.scatter(x3,y3,marker='.')
x3s=0
y3s=0
x4=np.linspace(-5,5,num=75)
y4=np.ones(75)*20
x4=np.append(x4,np.ones(125)*-5)
y4=np.append(y4,np.linspace(20,-10,num=125))
x4=np.append(x4,np.linspace(-5,25,num=150))
y4=np.append(y4,np.ones(150)*-10)
x4=np.append(x4,np.ones(50)*25)
y4=np.append(y4,np.linspace(-10,0,num=50))
x4=np.append(x4,np.ones(100)*5)
y4=np.append(y4,np.linspace(20,0,num=100))
plt.scatter(x4,y4,marker='.')
x4s=0
y4s=-5
x5=np.linspace(-5,5,num=50)
y5=np.ones(50)*20
x5=np.append(x5,np.ones(100)*-5)
y5=np.append(y5,np.linspace(20,-10,num=100))
x5=np.append(x5,np.linspace(-5,25,num=150))
y5=np.append(y5,np.ones(150)*-10)
x5=np.append(x5,np.ones(100)*25)
y5=np.append(y5,np.linspace(-10,0,num=100))
x5=np.append(x5,np.linspace(25,5,num=100))
y5=np.append(y5,np.ones(100)*0)
plt.scatter(x5,y5,marker='.')
x5s=5
y5s=-5
xs=[x1s,x2s,x3s,x4s,x5s]
ys=[y1s,y2s,y3s,y4s,y5s]
plt.scatter(xs,ys,marker='o')
plt.ylabel("2D Room Y position", fontsize=16)
plt.xlabel("2D Room X position", fontsize=16)
plt.title("Fake Room Layout", fontsize=22)
plt.show()
#As you can see, 5 sweeps were included in 5 different locations



#creation of the reading file
i=5
j=0
x=[x1,x2,x3,x4,x5]
y=[y1,y2,y3,y4,y5]
fin=np.array([j,500]) #including the header for the first reading
flight=np.array([j,1]) #including the header for the first position
while j<i:
    ret=write_reading(x[j],y[j],xs[j],ys[j])
    ret_a=np.array(ret)
    ret_a=ret_a.transpose()
    if(j>0):
        ret_a=np.vstack([np.array([j,500]),ret_a]) #including the header for each reading
        f_tmp=np.vstack([np.array([j,1]),np.array( [xs[j],ys[j]] ) ]) #including the header for each position
    else:
        f_tmp=np.array([xs[j],ys[j]])
    flight=np.vstack([flight,f_tmp])
    fin=np.vstack([fin,ret_a])
    j+=1
df=pd.DataFrame(fin) #loading the data frames into the csv file
df.to_csv('LIDARDPoints.csv',index=None,header=False,sep='\t')
df=pd.DataFrame(flight)
df.to_csv('Fake_flight.csv',index=None,header=False,sep='\t')
