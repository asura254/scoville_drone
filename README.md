Programming Task for the Scoville interview.
I chose to do the first 2 tasks, the display and simulation.
Running the programs is really simple, all you have to do is "python3 display.py" or "python3 Simulation.py".
 
For the display I put a small interface so the user can have some freedom choosing what they want to see. The code itself outputs the information necessary, but basically when starting the code you get an option to: see one specific sweep, see a superposition os sweeps where you can choose how many sweeps you want to see (from 0 to 33).
 
For the Simulation one, I chose to input the dimensions of the room directly from the code. When you run the code the room is drawn on the screen, and after you close the drawing the lidar points will be created in a csv file on your directory.